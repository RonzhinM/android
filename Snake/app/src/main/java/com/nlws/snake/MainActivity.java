package com.nlws.snake;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.nlws.snake.Engine.GameEngine;
import com.nlws.snake.Enums.Direction;
import com.nlws.snake.Enums.GameState;
import com.nlws.snake.Views.GameView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    GameEngine gameEngine;
    GameView gameView;
    private final long delay = 500;
    private final Handler handler = new Handler();

    float start_x;
    float start_y;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gameEngine = new GameEngine(42, 28);
        gameView = findViewById(R.id.GAME_VIEW);
        gameView.setMap(gameEngine.getMap());
        gameView.invalidate();
        gameView.setOnTouchListener(this);
        startUpdateHandler();
    }

    private void startUpdateHandler() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                gameEngine.update();
                GameState state = gameEngine.getState();
                if (state == GameState.RUNNING) {
                    handler.postDelayed(this, delay);
                }
                else if (state == GameState.LOSE) {
                    gameLost();
                }
                else if (state == GameState.WINNER) {
                    gameWinner();
                }
                gameView.setMap(gameEngine.getMap());
                gameView.invalidate();
            }
        }, delay);
    }

    private void gameWinner() {
        Toast.makeText(this, "You win", Toast.LENGTH_SHORT).show();
    }
    private void gameLost() {
        Toast.makeText(this, "You lost", Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: action_down(event); break;
            case MotionEvent.ACTION_UP: action_up(event); break;
        }
        return true;
    }
    private void action_up(MotionEvent event) {
        float dif_x = event.getX() - start_x;
        float dif_y = event.getY() - start_y;
        Direction direction;
        if ( Math.abs(dif_x) > Math.abs(dif_y) ) {
            if ( dif_x > 0 ) direction = Direction.EAST;
            else direction = Direction.WEST;
        } else {
            if ( dif_y > 0 ) direction = Direction.SOUTH;
            else direction = Direction.NORTH;
        }
        gameEngine.direction = direction;
    }
    private void action_down(MotionEvent event) {
        start_x = event.getX();
        start_y = event.getY();
    }
}