package com.nlws.snake.Enums;

import android.graphics.Color;

public enum GameFigure {
    NOTHING(Color.WHITE),
    WALL(Color.BLACK),
    SNAKE_HEAD(Color.RED),
    SNAKE_TAIL(Color.GRAY),
    APPLE(Color.GREEN);
    private final int color;
    GameFigure(int color) { this.color = color; }
    public int getColor() { return color; }
}