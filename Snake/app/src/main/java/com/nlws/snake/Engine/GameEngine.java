package com.nlws.snake.Engine;

import android.graphics.Point;
import android.util.Log;

import com.nlws.snake.Enums.*;

public class GameEngine {
    public Direction direction;
    private Snake snake;
    private Map map;
    private Point apple;
    private boolean isExist = false;
    private Point tail;
    private GameState gameState = GameState.RUNNING;
    public GameEngine(int height, int width) {
        this.direction = Direction.NORTH;
        this.snake = new Snake(10,10);
        this.map = new Map(height, width);
    }
    public Map getMap() {
        map.update();
        for (Point part_snake : snake.getBody()) {
            map.setField(part_snake, GameFigure.SNAKE_TAIL);
        }
        map.setField(snake.getHead(), GameFigure.SNAKE_HEAD);
        if ( !isExist ) { apple = map.newApple(); isExist = true; }
        map.setField(apple, GameFigure.APPLE);
        return map;
    }
    public GameState getState() { return gameState; }
    public void update() {
        snake.moveSnake(direction);
        if (tail != null) {
            snake.addTail(tail);
            tail = null;
        }
        Point head_snake = snake.getHead();
        for (Point part_snake : snake.getBody()) {
            if (part_snake.equals(head_snake)) { gameState = GameState.LOSE; return; }
        }
        if (head_snake.equals(apple)) {
            isExist = false;
            tail = snake.getTail();
        }
        for (Point wall : map.walls) {
            if (head_snake.equals(wall)) { gameState = GameState.LOSE; return; }
        }
        if (snake.getBody().size() > map.width * map.height / 2) { gameState = GameState.WINNER; return; }
    }
}