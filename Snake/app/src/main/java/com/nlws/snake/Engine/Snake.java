package com.nlws.snake.Engine;

import android.graphics.Point;
import com.nlws.snake.Enums.Direction;
import java.util.ArrayList;
import java.util.List;

public class Snake {
    private List<Point> snake;
    Snake(int x, int y) {
        snake = new ArrayList<>();
        addSnake(x, y);
    }
    public void moveSnake(Direction direction) {
        for ( int i = snake.size() - 1; i > 0 ; --i ) {
            Point part_snake = snake.get(i);
            Point next_part_snake = snake.get(i - 1);
            part_snake.y = next_part_snake.y;
            part_snake.x = next_part_snake.x;
        }
        Point head = getHead();
        switch (direction) {
            case WEST: head.x -= 1; break;
            case EAST: head.x += 1; break;
            case NORTH: head.y -= 1; break;
            case SOUTH: head.y +=1; break;
        }
    }
    private void addSnake(int start_x, int start_y) {
        snake.clear();
        snake.add(new Point(start_x, start_y));
        snake.add(new Point(start_x + 1, start_y));
        snake.add(new Point(start_x + 2, start_y));
        snake.add(new Point(start_x + 3, start_y));
    }
    public Point getHead() { return snake.get(0); }
    public Point getTail() { return snake.get(snake.size() - 1); }
    public List<Point> getBody() { return snake.subList(1, snake.size() - 1); }
    public void addTail(Point tail) { snake.add(tail); }
}