package com.nlws.snake.Engine;

import android.graphics.Point;
import com.nlws.snake.Enums.GameFigure;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Map {
    public final int height;
    public final int width;
    public final List<Point> walls;
    final GameFigure[][] map;
    Map(int height, int width) {
        this.height = height;
        this.width = width;
        map = new GameFigure[height][width];
        walls = new ArrayList<>();
        addWalls();
        update();
    }
    private void addWalls() {
        for (int y = 0; y < height; y++) {
            walls.add(new Point(0, y));
            walls.add(new Point(width - 1, y));
        }
        for (int x = 0; x < width; x++) {
            walls.add(new Point(x, 0));
            walls.add(new Point(x, height - 1));
        }
    }
    public void update() {
        for (GameFigure[] gameFigures : map) {
            Arrays.fill( gameFigures , GameFigure.NOTHING );
        }

        for (Point wall : walls) {
            map[wall.y][wall.x] = GameFigure.WALL;
        }
    }
    public GameFigure getField(int y, int x) { return map[y][x]; }
    public void setField(Point field, GameFigure gameFigure) { map[field.y][field.x] = gameFigure; }

    public Point newApple() {
        int x = 1 + new Random().nextInt(width - 2);
        int y = 1 + new Random().nextInt(height - 2);
        return new Point(x, y);
    }
}