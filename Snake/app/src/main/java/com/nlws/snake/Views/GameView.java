package com.nlws.snake.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.nlws.snake.Engine.Map;

public class GameView extends View {

    private Map map;
    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public void setMap(Map map) { this.map = map; }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (map == null) return;

        float blockSizeY = getHeight() / (float) map.height;
        float blockSizeX = getWidth() / (float) map.width;
        float circleRadius = Math.min(blockSizeX, blockSizeY) / 2;

        float shift_x = ( getWidth() - circleRadius * 2 * map.width ) / 2;
        float shift_y = ( getHeight() - circleRadius * 2 * map.height ) / 2;

        Paint paint = new Paint();
        for (int y = 0; y < map.height; y++) {
            for (int x = 0; x < map.width; x++) {
                paint.setColor( map.getField(y, x).getColor() );
                canvas.drawCircle(
                        0 + blockSizeX * x + circleRadius,
                        shift_y + blockSizeY * y + circleRadius,
                        circleRadius,
                        paint
                );
            }
        }
    }
}